#Author: Kevin López Ubau, 05/26/2019.
import time
import requests
import asyncio
from config import TOKEN	#create config.py file, add TOKEN variable with a bearer token value.
#from sense_hat import SenseHat
#from modelo import YoloTraining
#from ImplementaciónModeloOpenCV Python_yolo_opencv import *
import multiprocessing as mp
from telegram.ext import Updater, InlineQueryHandler, CommandHandler
from telegram import ChatAction
import requests
import re
import time
import os
from threading import Thread


def getTemp():
    url = 'http://192.168.43.70:5000/temp'
    response = requests.get(url)
    print(response)
    print(response.json())
    data = response.json()
    return data

def getHum():
        
	url = 'http://192.168.43.70:5000/hum'
	response = requests.get(url)
	print("acacacac")
	print(response)
	print(response.json())
	data = response.json() 
	return data
##################################################################



lista=[]


class BotManager:

    def __init__ (self,token,clientsFileName,url):
        self.apiUrl = url.format(token)

        print("url formateada ... " + self.apiUrl)
        
        self.clientsFileName = clientsFileName
        self.clients=[]
        self.updater= Updater(token)
        self.dispatcher = self.updater.dispatcher
        self.loadClientsFromFile()
        self.initUpdater()
        # otro hilo para el proceso que consultará un api y notificara a las personas vía telegram
       # threading.Thread(target=self.initUpdater, args=()).start()
        
        

    def loadClientsFromFile(self):

        print("Cargando chatIDs")
        file = open(self.clientsFileName,"r")
        lines = file.readlines()

        #clean clients list
        self.clients = []
        for line in lines:

            self.clients.append(int(line))

        print("clients in list")
        print(self.clients)
        file.close()

    def addClientToFile(self,fileName,chatID):

        file = open(fileName,"a")

        file.write(str(chatID)+"\n")

        file.close()


    def getFileLines(self, fileName):

        file= open(fileName,"r")

        lines = file.readlines()

        file.close()
        
        return lines
    
    def deleteClientFromFile(self,fileName,chatID):

        print("Eliminar chat con el ID: " + str(chatID))
        
        chatID = str(chatID)
        lines = self.getFileLines(self.clientsFileName)

        file = open(fileName,"w")
        print(lines)

        for line in lines:
            #remove \n from line and compare with chatID
            if line.rstrip() != chatID:
                
                file.write(line)

        file.close()
        

    def initUpdater(self):

        print("BOT INICIALIZADO")

        self.dispatcher.add_handler(CommandHandler('subscribeme',self.subscribeClient))
        self.dispatcher.add_handler(CommandHandler('unsubscribeme',self.unsubscribeClient))
        self.dispatcher.add_handler(CommandHandler('temperature',self.getTemperature))
        self.dispatcher.add_handler(CommandHandler('humidity',self.getHumidity))
        self.dispatcher.add_handler(CommandHandler('prueba',self.prueba))
        
        self.updater.start_polling()
        self.updater.idle()

    def sendPhotoMessage(self,bot,chatID,url):

        bot.sendChatAction(chat_id=chatID, action=ChatAction.UPLOAD_PHOTO)
        bot.sendDocument(chat_id=chatID, document=url)

    def sendTextMessage(self,bot,chatID,msg):

        print("Enviando el mensaje al cliente")
        bot.sendMessage(chat_id=chatID, text=msg)

    def unsubscribeClient(self,bot,update):

        chatID = update.message.chat_id

        if chatID in self.clients:
            print("clientes")
            print(self.clients)
            self.clients.remove(chatID)
            self.deleteClientFromFile(self.clientsFileName,chatID)
            print("luego de eliminar")
            print(self.clients)
            
            self.sendTextMessage(bot,chatID,"Gracias por usar nuestro servicio.")
            self.sendPhotoMessage(bot,chatID,"https://media1.giphy.com/media/9eM1SWnqjrc40/giphy.gif")

        else:
            self.sendTextMessage(bot,chatID,"Usted no se encuentra suscrito a mi servicio.")
    
    def subscribeClient(self,bot,update):

        chatID = update.message.chat_id
        print(chatID)
        print(self.clients)
        if chatID not in self.clients:
            self.clients.append(chatID)
            self.addClientToFile(self.clientsFileName,chatID)
            self.sendTextMessage(bot,chatID,"Bienvenido a mi servicio. Te estaremos notificando acerca de eventos de tu interés.")
            self.sendPhotoMessage(bot,chatID,"https://thumbs.gfycat.com/GrimyBetterAdder-max-1mb.gif")

        else:
            self.sendTextMessage(bot,chatID,"Usted ya se encuentra suscrito a mi servicio.")
        
    def getTemperature(self,bot,update):

        print("en comando get temperature")
        chatID = update.message.chat_id

        tempValue="Temperature: "+str(getTemp())+"°C"
        self.sendTextMessage(bot,chatID,tempValue)

    
    def getHumidity(self,bot,update):
            
        chatID = update.message.chat_id

        humValue="Humidity: "+str(getHum())+"%"
        self.sendTextMessage(bot,chatID,humValue)
        
    def prueba(self,bot,update):
        chatID=update.message.chat_id
        msg="ffff '\n' aaaa"
        self.sendTextMessage(bot,chatID,msg)
    def get_url(self):
        contents = requests.get('https://random.dog/woof.json').json()
        url = contents['url']
        return url


clientsFileName="clients.txt"
botToken ="778412308:AAFVtWVZKkK_aYOXrttMijqkwdPKPXeFL9Q" #"659619943:AAHBUUjpOHRvqwakTDR4pkM9IO6X-cpWPSU"#"744310760:AAHlgB7jHyeEm-CekSBX-plpVk9yAzOJ1Fc"
apiUrl = "https://api.telegram.org/bot{}/"

if __name__ == '__main__':

    fileExist = os.path.isfile("clients.txt")

    if fileExist == False:

        file = open(clientsFileName,"w")
        file.close()
    
    bot= BotManager(botToken,clientsFileName,apiUrl)
   