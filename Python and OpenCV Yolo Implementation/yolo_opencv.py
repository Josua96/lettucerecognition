#############################################
# Object detection - YOLO - OpenCV
# Author : Arun Ponnusamy   (July 16, 2018)
# Website : http://www.arunponnusamy.com
############################################


import cv2
import argparse
import numpy as np
import time
import requests
import urllib



configFile = "robosoccerv3-tiny-Testing.cfg"
weigthsFile = "robosoccerv3-tiny_final.weights"
classesFile =  "classesNames.txt"
inpWidth = 416
inpHeight = 416


url = "http://172.24.108.40:8080/shot.jpg"
#cap = cv2.VideoCapture(url)


conf_threshold = 0.7
nms_threshold = 0.4



classes = None

with open(classesFile, 'r') as f:
    classes = [line.strip() for line in f.readlines()]

COLORS = np.random.uniform(0, 255, size=(len(classes), 3))
    
def get_output_layers(net):
    
    layer_names = net.getLayerNames()
    
    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    return output_layers


def draw_prediction(img, class_id, confidence, x, y, x_plus_w, y_plus_h):

    label = str(classes[class_id])

    color = COLORS[class_id]
    print(x)
    print(y)
    print(x_plus_w)
    print(y_plus_h)

    cv2.rectangle(img, (x,y), (x_plus_w,y_plus_h), color, 2)

    cv2.putText(img, label, (x-10,y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)


def manageImageGeneration(image,boxes,class_ids,confidences,):

    
    
    indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)

    for i in indices:
        i = i[0]
        box = boxes[i]
        x = box[0]
        y = box[1]
        w = box[2]
        h = box[3]
        draw_prediction(image, class_ids[i], confidences[i], round(x), round(y), round(x+w), round(y+h))

    cv2.imwrite("object-detection.jpg", image)


def manageObjectDetections(image,outPut,width,height):

    
    class_ids = []
    confidences = []
    boxes = []
    

    for out in outPut:
        
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]

            # si el objeto detectado tiene un grado de confianza mayor a 0.70
            if confidence > 0.70:

                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)
                x = center_x - w / 2
                y = center_y - h / 2
                class_ids.append(class_id)
                confidences.append(float(confidence))
                boxes.append([x, y, w, h])

    manageImageGeneration(image,boxes,class_ids,confidences)

    
def makePrediction(image):

    width = image.shape[1]
    height = image.shape[0]
    scale = 0.00392
    

    net = cv2.dnn.readNet(weigthsFile, configFile)
    
    blob = cv2.dnn.blobFromImage(image, scale, (416,416), (0,0,0), True, crop=False)

    net.setInput(blob)

    outs = net.forward(get_output_layers(net))

    manageObjectDetections(image,outs,width,height)
    

def captureImages():

    while cv2.waitKey(1) < 0:

        try:
          #  cap = cv2.VideoCapture(url)
          #  _, img1 = cap.read()
            #si hay alguna imagen
          #  if(img1.any()):
                
                    
                    #hsv = cv2.cvtColor(img1, cv2.COLOR_BGR2HSV)
                    # hacer una conversion de colores para capturar en la imagen solo el color deseado
                   # lower_red = np.array([150,150,150])
                   # upper_red = np.array([155,155,155])
                  #  mask = cv2.inRange(hsv, lower_red, upper_red)
                  #  res = cv2.bitwise_and(img1, img1, mask=mask)

                   # cv2.imshow('img',img1)
                    #cv2.waitKey(5)
            imgResp = urllib.request.urlopen(url)
            imgNp = np.array(bytearray(imgResp.read()),dtype=np.uint8)
            img1=cv2.imdecode(imgNp,-1)
            
           #blob = cv2.dnn.blobFromImage(img1, 1 / 255, (inpWidth, inpHeight), [0, 0, 0], 1, crop=False) #frames from url
            makePrediction(img1)
                   # time.sleep(10)
                   
        except Exception as e:
            print(e)
            continue
                
              #  cv2.waitKey(100)
              #  cv2.destroyAllWindows()



captureImages()


""""

ap = argparse.ArgumentParser()
ap.add_argument('-i', '--image', required=True,
                help = 'path to input image')
ap.add_argument('-c', '--config', required=True,
                help = 'path to yolo config file')
ap.add_argument('-w', '--weights', required=True,
                help = 'path to yolo pre-trained weights')
ap.add_argument('-cl', '--classes', required=True,
                help = 'path to text file containing class names')
args = ap.parse_args()

image = cv2.imread(args.image)

Width = image.shape[1]
Height = image.shape[0]
scale = 0.00392

classes = None

with open(args.classes, 'r') as f:
    classes = [line.strip() for line in f.readlines()]

COLORS = np.random.uniform(0, 255, size=(len(classes), 3))


net = cv2.dnn.readNet(weightsFIle, configFile)

blob = cv2.dnn.blobFromImage(image, scale, (416,416), (0,0,0), True, crop=False)

net.setInput(blob)

outs = net.forward(get_output_layers(net))

class_ids = []
confidences = []
boxes = []
conf_threshold = 0.5
nms_threshold = 0.4
print("clases registradas")
print(classes)

print("en detecciones")
for out in outs:
    for detection in out:
        scores = detection[5:]
        class_id = np.argmax(scores)
        confidence = scores[class_id]
        if confidence > 0.5:
            print("deteccion con mas de 50% de confianza")
            print(detection)
            print("id de la clase "+str(class_id))
            print("Nombre de la clase " + str(classes[class_id]))
            center_x = int(detection[0] * Width)
            center_y = int(detection[1] * Height)
            w = int(detection[2] * Width)
            h = int(detection[3] * Height)
            x = center_x - w / 2
            y = center_y - h / 2
            class_ids.append(class_id)
            confidences.append(float(confidence))
            boxes.append([x, y, w, h])


indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)

for i in indices:
    i = i[0]
    box = boxes[i]
    x = box[0]
    y = box[1]
    w = box[2]
    h = box[3]
    draw_prediction(image, class_ids[i], confidences[i], round(x), round(y), round(x+w), round(y+h))

#cv2.imshow("object detection", image)
#cv2.waitKey()
    
cv2.imwrite("object-detection.jpg", image)

#cv2.destroyAllWindows()
"""""
