
#############################################
# Object detection - YOLO - OpenCV
# Author : Arun Ponnusamy   (July 16, 2018)
# Website : http://www.arunponnusamy.com
############################################
import json 
import telegram
#from pythonBot import clientsFileName, botToken
import argparse
import numpy as np
import cv2
import time
import requests
import urllib
import random
import socket,pickle
from threading import Thread
#from .pythonBot import BotManager
#import pythonBot
clientsFileName='clients.txt'
botToken="778412308:AAFVtWVZKkK_aYOXrttMijqkwdPKPXeFL9Q"
'''
configFile = "redesProyecto2V3Testing.cfg"#"robosoccerv3-tiny-Testing.cfg"
weigthsFile = "redesProyecto2V3_last.weights"#robosoccerv3-tiny_final.weights"
classesFile =  "redesProyecto2Names.txt"#"classesNames.txt"
inpWidth = 416
inpHeight = 416

#nivel de confianza para aceptar una detección como correcta
confidenceThreshold = 0.60



url = "http://192.168.43.70:8000/stream.mjpg"#"http://192.168.43.1:8080/shot.jpg"
PORT = 9000
BUFFER_SIZE = 1024

conf_threshold = 0.7
nms_threshold = 0.4


classes = None

with open(classesFile, 'r') as f:
    classes = [line.strip() for line in f.readlines()]

COLORS = np.random.uniform(0, 255, size=(len(classes), 3))
'''
URL = "https://api.telegram.org/bot{}/".format(botToken)
class YoloTraining:
    def __init__(self):
        self.configFile= "redesProyecto2V3Testing.cfg"
        self.weigthsFile="redesProyecto2V3_last.weights"
        self.classesFile="redesProyecto2Names.txt"
        self.inpWidth=416
        self.inpHeight=416
        self.confidenceThreshold = 0.60
        self.url="http://192.168.43.70:8000/stream.mjpg"
        self.PORT=9000
        self.BUFFER_SIZE=1024
        self.conf_threshold=0.7
        self.nms_threshold=0.4
        self.classes= None

        with open(self.classesFile, 'r') as f:
            self.classes = [line.strip() for line in f.readlines()]
        self.COLORS=np.random.uniform(0, 255, size=(len(self.classes), 3))






    def get_output_layers(self,net):

        layer_names = net.getLayerNames()

        output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

        return output_layers

    



    def draw_prediction(self, img, class_id, confidence, x, y, x_plus_w, y_plus_h):

        label = str(self.classes[class_id])

        color = self.COLORS[class_id]


        cv2.rectangle(img, (x,y), (x_plus_w,y_plus_h), color, 2)

        cv2.putText(img, label, (x-10,y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)


    def manageImageGeneration(self, image,boxes,class_ids,confidences,):



        indices = cv2.dnn.NMSBoxes(boxes, confidences, self.conf_threshold, self.nms_threshold)

        for i in indices:
            i = i[0]
            box = boxes[i]
            x = box[0]
            y = box[1]
            w = box[2]
            h = box[3]
            self.draw_prediction(image, class_ids[i], confidences[i], round(x), round(y), round(x+w), round(y+h))
        print("escribiendo imagen")
        cv2.imwrite("object-detection.jpg", image)



    def manageObjectDetections(self, image,outPut,width,height):


        class_ids = []
        confidences = []
        boxes = []
        totalReady=0


        print("administrando detecciones")
        for out in outPut:

            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]

                # si el objeto detectado tiene un grado de confianza mayor a 0.70
                if confidence > self.confidenceThreshold:
                    print("a")
                    print(self.classes[class_id])
                    totalReady+=1
                    
                    #sendMessage()
                    #if self.classes[0]:
                       #BotManager.lettuceReady()
                    print("b")

                    center_x = int(detection[0] * width)
                    center_y = int(detection[1] * height)
                    w = int(detection[2] * width)
                    h = int(detection[3] * height)
                    x = center_x - w / 2
                    y = center_y - h / 2
                    class_ids.append(class_id)

                    confidences.append(float(confidence))
                    boxes.append([x, y, w, h])

        print("llamada escribir imagenes")
        self.manageImageGeneration(image,boxes,class_ids,confidences)
        print(totalReady)
        if totalReady>0:
            self.send_message(totalReady)
            time.sleep(15)
            #self.sendImage()



    def makePrediction(self, image):


        #width = image.shape[1]
        #height = image.shape[0]
        width = self.inpWidth
        height = self.inpHeight
        scale = 0.00392


        net = cv2.dnn.readNet(self.weigthsFile, self.configFile)

        blob = cv2.dnn.blobFromImage(image, scale, (self.inpWidth,self.inpHeight), (0,0,0), True, crop=False)

        #para que el tamaño de la imagen a generar sea como lo indican las variables inpWidth y inpHeight
        resized = cv2.resize(image, (self.inpWidth,self.inpHeight), interpolation = cv2.INTER_AREA)

        net.setInput(blob)

        outs = net.forward(self.get_output_layers(net))
        print("a adminsitrar detecciones")
        self.manageObjectDetections(resized,outs,width,height)


    def captureImages(self):

        while cv2.waitKey(1) < 0:
            try:

                print("detecting image")

                cap = cv2.VideoCapture(self.url)
                _, img1 = cap.read()
                #si hay alguna imagen
                if(img1.any()):


                    #cv2.imshow('img',img1)
                    print("llamada a hacer predicciones")
                    self.makePrediction(img1)
                        #hsv = cv2.cvtColor(img1, cv2.COLOR_BGR2HSV)
                        # hacer una conversion de colores para capturar en la imagen solo el color deseado
                       # lower_red = np.array([150,150,150])
                       # upper_red = np.array([155,155,155])
                      #  mask = cv2.inRange(hsv, lower_red, upper_red)
                      #  res = cv2.bitwise_and(img1, img1, mask=mask)

                       # cv2.imshow('img',img1)
                        #cv2.waitKey(5)




               # imgResp = urllib.request.urlopen(url)
               # imgNp = np.array(bytearray(imgResp.read()),dtype=np.uint8)
            #img1=cv2.imdecode(imgNp,-1)

            except Exception as e:
                print(e)
                continue

    def get_url(self,url):
        response = requests.get(url)
        content = response.content.decode("utf8")
        return content
    


    def send_message(self,totalReady):
        msg="****************Attention**************** \nTotal of ready lettuces: "+str(totalReady)
        #bot=telegram.Bot(botToken)
        file = open(clientsFileName,"r")
        lines = file.readlines()

        #clean clients list
        clients = []
        for line in lines:

            clients.append(int(line))

        print("clients in list")
        print(clients)
        file.close()
        for i in clients:
            #print(i)
            
            url = URL + "sendMessage?text={}&chat_id={}".format(msg, i)
            self.get_url(url)
            time.sleep(10)
model=YoloTraining()
model.captureImages()
