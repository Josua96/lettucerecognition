#Author: Kevin López Ubau, 05/26/2019.
import time
import requests
import asyncio
from config import TOKEN	#create config.py file, add TOKEN variable with a bearer token value.
from sense_hat import SenseHat

TEMP_SENSOR_ID = '82e755c0-7f79-11e9-b4eb-6bf2c2412b24'
HUM_SENSOR_ID = '842023e0-7f79-11e9-b4eb-6bf2c2412b24'
BLUE_COLOR =[0,0,255]
RED_COLOR = [255,0,0]
SLEEP_TIME = 0.25
MAX_HUMIDITY = 30
MAX_TEMP = 38
sense = SenseHat()

temp = sense.get_temperature()
temp = round(temp, 2)
humidity = sense.get_humidity()
humidity = round(humidity, 2)

print('Temperature C°: {} (not accurate due to CPU heat!)'.format(temp))
print('Humidity: {} %'.format(humidity))

def maxData(actualData, maxAllowed):
	return True if actualData>maxAllowed else False

def drawBlue():
	sense.clear()
	sense.set_pixel(0, 0, BLUE_COLOR)
	sense.set_pixel(1, 1, BLUE_COLOR)
	sense.set_pixel(2, 2, BLUE_COLOR)
	sense.set_pixel(3, 3, BLUE_COLOR)

def drawRed():
	sense.clear()
	sense.set_pixel(4, 4, RED_COLOR)
	sense.set_pixel(5, 5, RED_COLOR)
	sense.set_pixel(6, 6, RED_COLOR)
	sense.set_pixel(7, 7, RED_COLOR)

def getCayenneData(sendorID):
	head = { 'Authorization': 'Bearer {}'.format(TOKEN) }
	url = 'https://platform.mydevices.com/v1.1/telemetry/a27250a0-7f76-11e9-ace6-4345fcc6b81e/sensors/{}/summaries?type=latest'.format(sendorID)
	response = requests.get(url, headers=head)

	print(response)
	print(response.json())
	data = response.json() #e.g. of a response: [{'ts': '2019-05-26T22:00:42.618Z', 'unit': 'c', 'v': 35.16204833984375, 'device_type': 'analog'}]
	value = data[0]["v"]

	return value

def getWeather():
	sense.clear()
	while True:
		temp = getCayenneData(TEMP_SENSOR_ID)		#sense.get_temperature()
		humidity = getCayenneData(HUM_SENSOR_ID)	#sense.get_humidity()
		sense.clear()
		if (maxData(humidity, MAX_HUMIDITY)):
			drawBlue()
		time.sleep(SLEEP_TIME)
		sense.clear()
		if (maxData(temp, MAX_TEMP)):
			drawRed()
		time.sleep(SLEEP_TIME)

getWeather()